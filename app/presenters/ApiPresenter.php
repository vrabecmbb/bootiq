<?php



namespace App\Presenters;


class ApiPresenter extends \Nette\Application\UI\Presenter{
    
    /** @var array() */
    public $branches = [];
    
    public function __construct() {
        parent::__construct();
        $branches = new \Branches(HomepagePresenter::DPD_API);
        $this->branches = $branches->getBranches();
    }
    
    /**
     * GET /api/get-all
     */
    public function actionGetAll(){
        $response = new \App\Model\Response('success', 200, $this->branches);       
        
        $this->sendJson($response);
    }
    
    /**
     * GET /api/get/<id>
     */
    public function actionGet($id){
        
        if(!$id){
            $response = new \App\Model\Response('error',400,"Identifiaction not typed");
            $this->sendJson($response);
        }
        
        foreach ($this->branches as $branch){
            if ($branch->getInternalId() == $id){
                $response = new \App\Model\Response('success',200,$branch);
                $this->sendJson($response);
            }
        }
        $response = new \App\Model\Response('error',404,"Branch not found");
        $this->sendJson($response);
    }
    
}
