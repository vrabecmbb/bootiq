<?php

namespace App\Presenters;

use Nette;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    const DPD_API = 'http://www.dpdparcelshop.cz/api/get-all';
    
    /** @var \App\Model\GeoJson @inject */
    public $geoJsonModel;
    
    /** @var array() */
    public $shops = [];
    
    public function __construct() {
        parent::__construct();
        $branches = new \Branches(self::DPD_API);
        $this->shops = $branches->getBranches();
    }
    
    public function actionDefault(){

        
    }    
    
    public function actionJson(){
       $geojson = $this->geoJsonModel->prepareGeoJson($this->shops);
       
       $this->sendJson($geojson);
    }
}
