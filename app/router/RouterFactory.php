<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
                $router = new RouteList;
            
                $router[] = new Route('geojson', 'Homepage:json');
                $router[] = new Route('/api/get-all', 'Api:getAll');
                $router[] = new Route('api/<action>[/<id>]', 'Api:default');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
                
		return $router;
	}
}
