<?php


namespace App\Model;


class Response implements \JsonSerializable{
    private $status;
    
    private $data;
    
    private $code;
    
    public function __construct($status, $code, $data) {
        $this->status = $status;
        $this->data = $data;
        $this->code = $code;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getData() {
        return $this->data;
    }

    public function getCode() {
        return $this->code;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setCode($code) {
        $this->code = $code;
    }
    
    public function jsonSerialize() {
        return [
            'status' => $this->status,
            'code' => $this->code,
            'data' => $this->data
        ];
    }

}
