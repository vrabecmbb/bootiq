<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BusinessHour
 *
 * @author marti
 */
class BusinessHourModel implements JsonSerializable{

    /**
     * @var string
     */
    private $dayOfWeek;

    /**
     * @var string
     */
    private $businessHour;
    
    public function __construct($dayOfWeek, $businessHour) {
        $this->dayOfWeek = $dayOfWeek;
        $this->businessHour = $businessHour;
    }
    
    public function getDayOfWeek() {
        return $this->dayOfWeek;
    }

    public function getBusinessHour() {
        return $this->businessHour;
    }

    public function setDayOfWeek($dayOfWeek) {
        $this->dayOfWeek = $dayOfWeek;
    }

    public function setBusinessHour($businessHour) {
        $this->businessHour = $businessHour;
    }
    
    public function jsonSerialize() {
        return [
            'dayOfWeek' => $this->dayOfWeek,
            'businessHour' => $this->businessHour
        ];
    }
}
