<?php

namespace App\Model;

class GeoJson {
    
    private $geoJson = [];
    
    public function prepareGeoJson(array $items){
        
        $features = [];
        
        $geoJson = [
            'type' => 'FeatureCollection',
            'features' => []
        ];
        
        
        foreach ($items as $item){
            
            $location = $item->getLocation();
            
            $feature = [
                'id' => $item->getInternalId(),
                'type' => 'Feature',
                'geometry' => [
                    'type' => 'Point',
                    'coordinates' => [$location->getLongitude(),$location->getLatitude()]
                ],
                'properties' => [
                    'name' => $item->getInternalName(),
                    'address' => (string)$item->getAddress(),
                    'business_hours' => $this->prepareBusinessHours($item->getBusinessHours())
                ]
            ];
            
            $features[] = $feature;
            
        }
        
        $geoJson['features'] = $features;
       
        return $geoJson;
        
    }
    
    private function prepareBusinessHours(array $businessHours){
        
        $toRet = [];
        
        foreach($businessHours as $day){
            $toRet[] = [$day->getDayOfWeek(),$day->getBusinessHour()];
        }
        
        return $toRet;
    }
    
    
    
}
