<?php


class Branch implements JsonSerializable{

    /**
     * @var string
     */
    private $internalId;

    /**
     * @var string
     */
    private $internalName;

    /**
     * @var Coordinates
     */
    private $location;

    /**
     * @var array|BusinessHourModel[]
     */
    private $businessHours;

    /**
     * @var Address
     */
    private $address;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $email;
    
    public function __construct($internalId, $internalName, $phoneNumber, $email) {
        $this->internalId = $internalId;
        $this->internalName = $internalName;
        $this->phoneNumber = $phoneNumber;
        $this->email = $email;
    }

    
    public function getInternalId() {
        return $this->internalId;
    }

    public function getInternalName() {
        return $this->internalName;
    }

    public function getLocation() {
        return $this->location;
    }

    public function getBusinessHours() {
        return $this->businessHours;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setInternalId($internalId) {
        $this->internalId = $internalId;
    }

    public function setInternalName($internalName) {
        $this->internalName = $internalName;
    }

    public function setLocation(Coordinates $location) {
        $this->location = $location;
    }

    public function setBusinessHours(array $businessHours) {
        $this->businessHours = $businessHours;
    }

    public function setAddress(Address $address) {
        $this->address = $address;
    }

    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function jsonSerialize()
    {
        return [
            'internalId' => $this->internalId,
            'internalName' => $this->internalName,
            'location' => $this->location,
            'businessHours' => $this->businessHours,
            'address' => $this->address,
            'phoneNumber' => $this->phoneNumber,
            'email' => $this->email
        ];
    }


}
