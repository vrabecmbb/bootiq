<?php


class Branches {
    
    /** Array[Branch] */
    private $branches = [];
    
    public function __construct($url) {
        $reader = new \App\Model\ApiReader\ApiReader($url);
        
        try{
            $items = $reader->getItems();
            
            $this->prepareBranches($items);
               
        } catch (Exception $ex){
            $error = $ex->getMessage();
        }  
    }
    
    private function prepareBranches(array $items){
        
        foreach ($items as $item){
            
            $hours = [];
            
            $branch = new \Branch($item->id, $item->company, $item->phone, $item->email);
            $coordinates = new \Coordinates($item->latitude, $item->longitude);
            $address = new \Address($item->street, $item->house_number, $item->city, $item->postcode, $item->fax, $item->homepage);
            
            foreach($item->hours as $businessHour){
                $businessTime = $this->formatTime($businessHour);
                $hours[] = new \BusinessHourModel($businessHour->dayName, $businessTime);
            }
            $branch->setBusinessHours($hours);
            
            $branch->setLocation($coordinates);
            $branch->setAddress($address);
                    
            $this->branches[] = $branch;
        }
        
    }
    
    private function formatTime($businessHour){
        if (!empty($businessHour->openMorning) && !empty($businessHour->openAfternoon)){
            return sprintf("%s - %s %s - %s", $businessHour->openMorning, $businessHour->closeMorning, $businessHour->openAfternoon, $businessHour->closeAfternoon);
        } else if (!empty($businessHour->openMorning)){
            return sprintf("%s - %s", $businessHour->openMorning, $businessHour->closeMorning);
        } else {
            return "-";
        }
    }
    
    public function getBranches(){
        return $this->branches;
    }
}
