<?php

namespace App\Model\ApiReader;

use Matyx\Guzzlette\ClientFactory;
use GuzzleHttp\Client;

class ApiReader
{

    /* @var Client */
    private $client;
    
    public function __construct($url) {
        $this->url = $url;
        $this->client = new Client();
    }
    
    private function readData(){
        $result = $this->client->request('GET', $this->url);
        
        return \GuzzleHttp\json_decode($result->getBody()->getContents());
    }
    
    public function getItems(){
        $data = $this->readData();
        
        if ($data->status != "ok"){
            throw new Exception("API Call is wrong");
        }
        
        if (!isset($data->data->items)){
            throw new Exception("API Results are bad");
        }
        
        $items = $data->data->items;
        
        return $items;
    }
}