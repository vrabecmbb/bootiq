<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Address
 *
 * @author marti
 */
class Address implements JsonSerializable{
    
    /** @var string */
    private $street;
    
    /** @var string */
    private $city;
    
    /** @var string */
    private $houseNumber;
    
    /** @var string */
    private $postcode;
    
    /** @var string */
    private $fax;
    
    /** @var string */
    private $homepage;
    
    public function __construct($street, $houseNumber, $city, $postcode, $fax, $homepage) {
        $this->street = $street;
        $this->city = $city;
        $this->houseNumber = $houseNumber;
        $this->postcode = $postcode;
        $this->fax = $fax;
        $this->homepage = $homepage;
    }

    
    public function getStreet() {
        return $this->street;
    }

    public function getCity() {
        return $this->city;
    }

    public function getHouseNumber() {
        return $this->houseNumber;
    }

    public function getPostcode() {
        return $this->postcode;
    }

    public function getFax() {
        return $this->fax;
    }

    public function getHomepage() {
        return $this->homepage;
    }

    public function setStreet($street) {
        $this->street = $street;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function setHouseNumber($houseNumber) {
        $this->houseNumber = $houseNumber;
    }

    public function setPostcode($postcode) {
        $this->postcode = $postcode;
    }

    public function setFax($fax) {
        $this->fax = $fax;
    }

    public function setHomepage($homepage) {
        $this->homepage = $homepage;
    }
    
    public function __toString() {
        return sprintf("%s %s " . PHP_EOL . "%s %s ". PHP_EOL . "%s", $this->street, $this->houseNumber, $this->city, $this->postcode, $this->fax);
    }
    
    public function jsonSerialize() {
        return [
            'street' => $this->street,
            'city' => $this->city,
            'houseNumber' => $this->houseNumber,
            'postcode' => $this->postcode,
            'fax' => $this->fax,
            'homepage' => $this->homepage
        ];
    }
 
}
